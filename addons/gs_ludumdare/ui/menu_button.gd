extends Button

@export var to_state : Game.GameStates = Game.GameStates.NONE

func _on_MenuButton_pressed():
	LudumDare.menu_button_pressed.emit(to_state)
