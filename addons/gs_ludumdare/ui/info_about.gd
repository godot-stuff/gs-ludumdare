extends RichTextLabel


func _ready():

	var settings = LudumDare.game_settings as LudumSettingsResource
	if (settings):
		text = LudumDare.load_info(settings.about_info)
