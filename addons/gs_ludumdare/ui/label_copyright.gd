extends RichTextLabel

@onready var settings = LudumDare.game_settings as LudumSettingsResource

func _ready():
	if (settings):
		text = settings.copyright
