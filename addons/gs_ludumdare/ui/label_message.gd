extends RichTextLabel

@onready var settings = LudumDare.game_settings as LudumSettingsResource


func _process(delta):
	if (settings):
		position += Vector2.LEFT * settings.message_speed * delta
	
	
func _ready():
	
	var _message_text = "No Message"
	
	if (settings):
		_message_text = settings.message

	text = _message_text
	
	
	position = Vector2(get_viewport().size.x + 100, position.y)
