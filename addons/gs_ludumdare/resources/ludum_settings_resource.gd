class_name LudumSettingsResource
extends Resource

var name : String = "Some Game"
var version : String = "1.0"
var long_version : String = "Version 1.0"
var copyright : String = "Some Copyright"
var company : String = "Some Company"
var publisher : String = "Some Publisher"
var title : String = "Some Game Title"
var ludum_number : String = ""
var ludum_link : String = ""
var profile_link : String = ""
var itch_link : String = ""
var help_info : String = ""
var about_info : String = ""
var home_scene : String = ""
var about_scene : String = ""
var help_scene : String = ""
var play_scene : String = ""
var score_scene : String = ""
var option_scene : String = ""
var show_about : bool = false
var show_help : bool = false
var show_score : bool = false
var show_option : bool = false
var show_quit : bool = true
var message_speed : int = 100
var message : String = "Hello World . . ."
