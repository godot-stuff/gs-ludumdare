extends LudumScene

func _ready():
	
	if LudumDare.game_settings:
		
		$Node/VBoxContainer/OptionsButton.visible = LudumDare.game_settings.show_option
		$Node/VBoxContainer/AboutButton.visible = LudumDare.game_settings.show_about
		$Node/VBoxContainer/HelpButton.visible = LudumDare.game_settings.show_help
		$Node/VBoxContainer/ScoresButton.visible = LudumDare.game_settings.show_score
		$Node/VBoxContainer/QuitButton.visible = LudumDare.game_settings.show_quit
	
	super()
