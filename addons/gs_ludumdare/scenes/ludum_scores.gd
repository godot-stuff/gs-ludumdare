extends LudumScene

@export var score_panel : PackedScene 
@onready var score_list = $Node/ScoreList


func _ready():
	
	super._ready()

	var _panel
	
	_panel = score_panel.instantiate()
	_panel.get_node("$Player").text = "Player 1"
	_panel.get_node("$Score").text = "1000"
	score_list.add_child(_panel)

	_panel = score_panel.instantiate()
	_panel.get_node("$Player").text = "Player 2"
	_panel.get_node("$Score").text = "2000"
	score_list.add_child(_panel)
