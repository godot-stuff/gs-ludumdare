extends Node2D


signal menu_button_pressed(state : Game.GameStates)
signal scores_loaded()


const VERSION = "4.3"

var __game_settings_data: LudumSettingsResource

var game_settings: LudumSettingsResource:
	get: 
		return __game_settings_data 


func _on_game_process(state : int, delta : float) -> void:
	
	match state:
		
		Game.GameStates.INIT:
			_on_state_init()
		
		Game.GameStates.ABOUT:
			_on_state_about()
		
		Game.GameStates.HELP:
			_on_state_help()
		
		Game.GameStates.OPTIONS:
			_on_state_options()
		
		Game.GameStates.SCORES:
			_on_state_scores()
		
		Game.GameStates.HOME:
			_on_state_home()
		
		Game.GameStates.PLAY:
			_on_state_play()
		

func start_game(game_settings_filename : String = "res://ludam_dare.ini", user_settings_filename : String = "user://user.ini") -> void:

	Game.load_game_settings(game_settings_filename)
	Game.load_user_settings(user_settings_filename)
	
	__game_settings_data = LudumSettingsResource.new()
	
	# load standard game settings
	__game_settings_data.name = Game.get_game_setting("name")
	__game_settings_data.version = Game.get_game_setting("version")
	__game_settings_data.long_version = Game.get_game_setting("long_version")
	__game_settings_data.copyright = Game.get_game_setting("company")
	__game_settings_data.publisher = Game.get_game_setting("publisher")
	__game_settings_data.title = Game.get_game_setting("title")
	
	# ludum dare specific settings
	__game_settings_data.ludum_number = Game.get_game_setting("ludum_number")
	__game_settings_data.ludum_link = Game.get_game_setting("ludum_link")
	__game_settings_data.profile_link = Game.get_game_setting("profile_link")
	__game_settings_data.itch_link = Game.get_game_setting("itch_link")
	__game_settings_data.home_scene = Game.get_game_setting("home_scene")
	__game_settings_data.about_scene = Game.get_game_setting("about_scene")
	__game_settings_data.help_scene = Game.get_game_setting("help_scene")
	__game_settings_data.score_scene = Game.get_game_setting("score_scene")
	__game_settings_data.option_scene = Game.get_game_setting("option_scene")
	__game_settings_data.play_scene = Game.get_game_setting("play_scene")
	__game_settings_data.help_info = Game.get_game_setting("help_info")
	__game_settings_data.about_info = Game.get_game_setting("about_info")
	__game_settings_data.message = Game.get_game_setting("message")
	__game_settings_data.message_speed = Game.get_game_setting("message_speed")
	__game_settings_data.show_about = Game.get_game_setting("show_about")
	__game_settings_data.show_help = Game.get_game_setting("show_help")
	__game_settings_data.show_score = Game.get_game_setting("show_score")
	__game_settings_data.show_option = Game.get_game_setting("show_option")
	__game_settings_data.show_quit = Game.get_game_setting("show_quit")
	
	Game.on_game_process.connect(_on_game_process)
	Game.start_game()
	
	
func load_info(info : String):
	Logger.trace("[Ludum] load_info")

	var _info = info

#	if (_info.substr(0, 6) == "res://"):
#		var _file = FileAccess.new()
#		if (_file.file_exists(_info)):
#			_file.open(_info, FileAccess.READ)
#			_info = _file.get_as_text()

	return _info
	
	
func _on_state_init():
	Logger.trace("[LudumDare] _on_state_init")
	Game.set_state(Game.GameStates.HOME)


func _on_state_home():
	Logger.trace("[LudumDare] _on_state_home")
	SceneManager.transition_to(__game_settings_data.home_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_state_about():
	Logger.trace("[LudumDare] _on_state_about")
	SceneManager.transition_to(__game_settings_data.about_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_state_help():
	Logger.trace("[Ludum] _on_state_help")
	SceneManager.transition_to(__game_settings_data.help_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_state_play():
	Logger.trace("[LudumDare] _on_state_play")
	SceneManager.transition_to(__game_settings_data.play_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_state_scores():
	Logger.trace("[LudumDare] _on_state_scores")
	SceneManager.transition_to(__game_settings_data.score_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_state_options():
	Logger.trace("[LudumDare] _on_state_options")
	SceneManager.transition_to(__game_settings_data.option_scene)
	Game.set_state(Game.GameStates.IDLE)


func _on_menu_button_pressed(state: Game.GameStates):
	Game.set_state(state)
	
	
func _init():
	
	Logger.trace("[LudumDare] _init")
	
	print()
	print("godot-stuff Ludum")
	print("https://gitlab.com/godot-stuff/gs-ludumdare")
	print("Copyright 2018-2023, SpockerDotNet LLC")
	print("Version " + VERSION)
	print(" ")
	
	
	
func _ready():
	
	Logger.trace("[LudumDare] _ready")
	
	menu_button_pressed.connect(_on_menu_button_pressed)
	
	Game.load_game_settings("res://addons/gs_ludumdare/ludum_dare.ini")
