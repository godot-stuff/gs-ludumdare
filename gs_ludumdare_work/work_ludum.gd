extends Node2D

var __settings : LudumSettingsResource

func _ready():
	
	LudumDare.start_game("res://gs_ludumdare_work/game.ini")
	
	Game.show_game_settings()
	
	__settings = LudumDare.game_settings as LudumSettingsResource
	
	Logger.info(__settings.name)
	Logger.info(__settings.home_scene)
